#!/bin/bash
# set -e


function dc_down() {
    docker-compose down
}

function dc_up() {
    docker-compose up -d
}

if [ "$ENV" = "DEV" ]; then
    echo "Running Development Application."
    dc_down
    dc_up

elif [ "$ENV" = "REBUILD" ]; then
    echo "Rebuild Application."
    dc_down
    dc_up
    docker-compose build --no-cache

elif [ "$ENV" = "UNIT_TEST" ]; then
    echo "Running Unit Tests."
    dc_down
    dc_up
    python -m pytest ./test_unit   

elif [ "$ENV" = "INTEGRATION_TEST" ]; then
    echo "Running Integration Tests."
    dc_down
    dc_up
    python -m pytest ./test_integration

elif [ "$ENV" = "STAGE" ]; then
    echo "Deploying Staging Environment."
    # push built containers to private docker hub
    # exec some script, so that stage environment pulls new containers
    # exec some script, so that new containers are fired up
    # exec some announcement script that fresh stage is available
    
elif [ "$ENV" = "PROD" ]; then
    echo "Deploying Production Environment."
    # 

else
    echo "Please provide an environment."
    echo "Stopping without having done anything."
fi
