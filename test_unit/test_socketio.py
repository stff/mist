import json
import socketio


class TestNamespace(socketio.ClientNamespace):
    def on_connect(self):
        self.emit('room', {'room': 'abc123'})

    def on_disconnect(self):
        pass

    def on_message(self, data):
        print(json.dumps(data, indent=2))

    def on_reply(self, data):
        print('Hooray!')
        # self.emit('test', data)

    def ack(self):
        print('delivered to miradock.')


# -----------------
def send_message(event, data, nsp='/mira'):
    sio.emit(event, data, namespace=nsp)


# -----------------
sio = socketio.Client()
sio.register_namespace(TestNamespace('/mira'))
sio.connect('http://localhost')
