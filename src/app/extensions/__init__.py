
from flask_pymongo import PyMongo
from flask_socketio import SocketIO
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_debugtoolbar import DebugToolbarExtension

# Database connection
mongo = PyMongo()
mdb = dict()
# db_maintenance = PyMongo()
# db_measurement = PyMongo()
# db_operation = PyMongo()


# SocketIO connection
socketio = SocketIO()


# Some rate limiting to prevent DDoS
rate_limiter = Limiter(
    key_func=get_remote_address,
    default_limits=['500/hour']
)


# debug toolbar (only active in flask debug mode)
debug_toolbar = DebugToolbarExtension()
