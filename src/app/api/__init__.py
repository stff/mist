
from flask import Blueprint
from flask_restful import Api


from app.api.resources.Index import Index


api_bp = Blueprint('api', __name__)
api_v1 = Api(api_bp)


api_v1.add_resource(Index, '/')
