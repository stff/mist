
from flask import render_template

from flask_restful import Resource


class Index(Resource):
    def get(self):
        return render_template('index.html')
