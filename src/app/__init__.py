
import os
import uuid
from flask import Flask


from app.celery import create_celery


def create_app(debug=False):
    app = Flask(__name__)
    app.debug = debug
    app.config['SECRET_KEY'] = os.environ.get('APP_SECRET_KEY', uuid.uuid4().hex)
    return app


def register_extensions(flask_app):
    from app.api import api_bp
    flask_app.register_blueprint(api_bp, url_prefix='/api')

    # init and register databases here
    from app.extensions import mongo, mdb
    db_host = os.environ.get('DATABASE_HOSTNAME')
    db_port = os.environ.get('DATABASE_PORT')
    flask_app.config['MONGO_URI'] = f"mongodb://{db_host}:{db_port}/"
    mongo.init_app(flask_app)
    # populate the mdb database dictionary
    mdb['measurement'] = mongo.cx[str(os.environ.get('DB_MEASUREMENT'))]
    mdb['maintenance'] = mongo.cx[str(os.environ.get('DB_MAINTENANCE'))]
    mdb['operation'] = mongo.cx[str(os.environ.get('DB_OPERATION'))]

    # init and register rate limiting
    from app.extensions import rate_limiter
    rate_limiter.init_app(flask_app)

    # init debug toolbar
    from app.extensions import debug_toolbar
    debug_toolbar.init_app(flask_app)

    # init and register SocketIO (always as last extension to be registered)
    from app.extensions import socketio
    backend_pass = os.environ.get('CELERY_BACKEND_PASSWORD', 'password')
    backend_host = os.environ.get('CELERY_BACKEND_HOST', 'redis')
    backend_port = os.environ.get('CELERY_BACKEND_PORT', 6379)
    redis_url = f"redis://:{backend_pass}@{backend_host}:{backend_port}"
    socketio.init_app(flask_app, message_queue=redis_url)


debug = bool(int(os.environ.get('MIRADOCK_DEBUG', 0)))
flask_app = create_app(debug=debug)

celery_app = create_celery(flask_app)
celery_app.autodiscover_tasks(['celery'])

register_extensions(flask_app)
