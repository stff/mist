
import datetime

from flask_socketio import Namespace
from jsonschema import validate, ValidationError


class Device(Namespace):
    def __init__(self, *args, **kwargs):
        self.manufacturer = kwargs.pop('manufacturer', 'generic_manufacturer')
        self.model = kwargs.pop('model', 'generic_model')
        super(Device, self).__init__(*args, **kwargs)

    @property
    def now(self):
        '''
        Returns an ISO 8601 time stamp string in UTC (Zulu) time.
        '''
        return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()

    def validate_schema(self, data, schema):
        '''
        Validates an incoming SocketIO message according to
        a specified JSON Schema v7.0.
        See: https://json-schema.org/understanding-json-schema/index.html

        :param data: Incoming data via SocketIO message
        :type data: `JSON object`
        :param schema: Schema for validation
        :type schema: `dict`
        '''
        try:
            validate(data, schema)
            data['valid_schema'] = True
        except ValidationError:
            data['valid_schema'] = False
        return data

    def get_room(self, data, idn_key='idn', serial_key='serial'):
        # TODO: this could be hi-jacked by pretending to be
        #       another device and then reverse engineer the
        #       handling routines.
        '''
        Returns the SocketIO room of the current device handle.

        :param data: Currently handeled message
        :type data: `dict`
        :param idn_key: Key to access the IDN embedded object
        :type idn_key: `str`
        :param serial_key: Key to access the serial number field in IDN object
        :type serial_key: `str`
        '''
        idn = data.get(idn_key, {})
        serial = idn.get(serial_key, 'generic_serial')
        room = f"{self.manufacturer}/{self.model}/{serial}"
        return room

    def get_collection(self, data, idn_key='idn', serial_key='serial'):
        '''
        Returns the collection name for storing the incoming message.

        :param data: Currently handeled message
        :type data: `dict`
        :param idn_key: Key to access the IDN embedded object
        :type idn_key: `str`
        :param serial_key: Key to access the serial number field in IDN object
        :type serial_key: `str`
        '''
        return self.get_room(data, idn_key, serial_key)
