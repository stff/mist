
from flask_socketio import join_room

from app.celery.tasks import useless_double_printer

from app.devices.device import Device
from .dataschemas import schema_maintenance, schema_measurements, schema_operation


class ExampleDevice(Device):
    def __init__(self, *args, **kwargs):
        super(ExampleDevice, self).__init__(*args, **kwargs)

    def on_connect(self):
        job = useless_double_printer("I am", "connected.")
        job.delay()

    def on_disconnect(self):
        pass

    def on_room(self, data):
        pass
