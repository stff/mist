

_idn_schema = {
    "type": "object",
    "properties": {
        "manufacturer": {"type": "string"},
        "model": {"type": "string"},
        "serial": {"type": "string"},
        "firmware": {"type": "string"}
    }
}

# validation schema for messages to 'room'
schema_room = {
    "type": "object",
    "properties": {
        "idn": _idn_schema,
    }
}

# validation schema for messages regarding maintenance
schema_maintenance = {
    "type": "object",
    "properties": {
        "idn": _idn_schema
    }
}

# validation schema for messages regarding operation data
schema_operation = {
    "type": "object",
    "properties": {
        "idn": _idn_schema
    }
}

# validation schema for messages regarding measurement data
schema_measurements = {
    "type": "object",
    "properties": {
        "idn": _idn_schema
    }
}
