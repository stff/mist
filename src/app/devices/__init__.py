
from app.extensions import socketio

from app.devices.example.ExampleDevice import ExampleDevice


def register_socketio_devices():
    socketio.on_namespace(
        ExampleDevice(namespace='/mira',
                      manufacturer='example GmbH',
                      model='example-model'))
