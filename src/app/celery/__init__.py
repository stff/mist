
import os

from celery import Celery


def create_celery(app):
    broker_user = os.environ.get('CELERY_BROKER_USER', 'default')
    broker_pass = os.environ.get('CELERY_BROKER_PASSWORD', 'default')
    broker_host = os.environ.get('CELERY_BROKER_HOST', 'rabbitmq')
    broker_port = os.environ.get('CELERY_BROKER_PORT', 5672)

    backend_pass = os.environ.get('CELERY_BACKEND_PASSWORD', 'password')
    backend_host = os.environ.get('CELERY_BACKEND_HOST', 'redis')
    backend_port = os.environ.get('CELERY_BACKEND_PORT', 6379)
    app.config.update(
        CELERY_BROKER=f"amqp://{broker_user}:{broker_pass}@{broker_host}:{broker_port}",
        CELERY_RESULT_BACKEND=f"redis://:{backend_pass}@{backend_host}:{backend_port}"
    )

    celery = Celery(
        app.import_name,
        backend=f"redis://{backend_host}:{backend_port}",
        broker=f"amqp://{broker_user}:{broker_pass}@{broker_host}:{broker_port}",
        include=['app.celery.tasks']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    celery.finalize()

    return celery
