
import json

from app import celery_app
from app.celery.config import CELERY_DEFAULT_TASK_TIMEOUT as TIMEOUT


@celery_app.task(name='printer', soft_time_limit=TIMEOUT)
def celery_printer(data):
    print(json.dumps(data, indent=2))
