
from celery import group

from app.celery.atomic.helpers import celery_printer
from app.celery.config import CELERY_DEFAULT_TASK_TIMEOUT as TIMEOUT


def useless_double_printer(s1, s2=None):
    if not s2:
        s2 = 'default_string'

    return group(
        [
            celery_printer.s(s1),
            celery_printer.s(s2)
        ]
    )
