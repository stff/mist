
import kombunicator

from app.celery.tasks import useless_double_printer
from app.communication.config import barbershop_connection_parameters


class ExampleMessageConsumer(kombunicator.ConsumerConfigurator):
    def configure(self):
        self.connection_parameter = barbershop_connection_parameters
        self.exchange_name = "example_exchange"
        self.binding_keys = ['key1']

    @staticmethod
    def message_handler(message):
        job = useless_double_printer("Print", "me")
        job.delay()
