
import os


barbershop_connection_parameters = {
    'hostname': os.environ.get('BARBERSHOP_HOST', 'barbershop'),
    'port': os.environ.get('BARBERSHOP_PORT', 5672),
    'userid': os.environ.get('BARBERSHOP_USER', 'guest'),
    'password': os.environ.get('BARBERSHOP_PASSWORD', 'guest')
}
