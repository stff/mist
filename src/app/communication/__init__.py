
import kombunicator


def start_message_consumers():
    from .ExampleMessageConsumer import ExampleMessageConsumer
    kombunicator.register_message_consumer(ExampleMessageConsumer)
