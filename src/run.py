
import os

from app import flask_app
from app.communication import start_message_consumers
from app.devices import register_socketio_devices
from app.extensions import socketio


if __name__ == '__main__':
    # start up backend communication
    start_message_consumers()

    # register all the SocketIO devices here
    register_socketio_devices()

    # start up flask app
    port = int(os.environ.get('MIST_PORT', 5000))
    socketio.run(flask_app, host='0.0.0.0', port=port)
