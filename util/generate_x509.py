
import shutil
import subprocess
import sys
import os

from socket import gethostname

from OpenSSL import crypto


CERTIFICATE = "certificate.crt"
PRIVATE_KEY = "certificate.key"


def pip_install(package):
    """
    This one calls the correct pip according to the
    currently executed python interpreter.
    """
    subprocess.call([sys.executable, "-m", "pip", "install", "--trusted-host", "pypi.python.org", package])


def create_certificate():
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 2048)

    cert = crypto.X509()
    cert.get_subject().C = "DE"
    cert.get_subject().ST = "BW"
    cert.get_subject().L = "Stuttgart"
    cert.get_subject().O = "jojojo GmbH"
    cert.get_subject().OU = "Software Carpentry"
    cert.get_subject().CN = f"{gethostname()}/email=stefanlasse87@gmail.com"

    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(365 * 24 * 60 * 60)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, 'sha256')

    with open(CERTIFICATE, 'wt') as fh:
        fh.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode())

    with open(PRIVATE_KEY, 'wt') as fh:
        fh.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode())


if __name__ == '__main__':
    create_certificate()
    os.makedirs('../ssl', exist_ok=True)
    shutil.move(CERTIFICATE, f'../ssl/{CERTIFICATE}')
    shutil.move(PRIVATE_KEY, f'../ssl/{PRIVATE_KEY}')
