
import json
import uuid
import random
import socketio
from time import sleep

# --------------------------------------------
# MIRA SocketIO simulation
sio = socketio.Client()


@sio.on('connect', namespace='/mira')
def on_connect():
    print('connected')


@sio.on('disconnect', namespace='/mira')
def on_disconnect():
    print('disconnected')


# --------------------------------------------
def send_event(data, event, nsp='/mira'):
    sio.emit(event, data, namespace=nsp)


def get_mira_idn():
    idn = dict()
    idn['manufacturer'] = "micro-biolytics GmbH"
    idn['model'] = "MIRA Analyzer"
    idn['serial'] = f"SN000{random.choice(list(range(1,5)))}"
    print(f"used {idn['serial']}")
    idn['firmware'] = "v08.15"
    return idn


# --------------------------------------------
def send_measurement(n=1):
    sio.connect('http://localhost')
    for x in range(0, n):
        with open('./test_EWA_measurement.json', 'r') as fh:
            ewa = {"measurement": json.loads(fh.read())}

            # changing uuid to simulate a diffrent measurment each time
            ewa["measurement"]["experimentStepID"] = str(uuid.uuid4())
            ewa['idn'] = get_mira_idn()

        sleep(3)
        send_event(ewa, event='measurement')

    sio.disconnect()


send_measurement()
