mist
====
A microservice template for future web service development.

Setup
-----
1. Checkout repository via SSH:  
`$ git clone git@gitlab.com:stff/mist.git`  
or via HTTP  
`$ git clone https://gitlab.com/stff/mist.git`

2. Generate your SSL certificates  
`$ cd mist/util && python generate_x509.py`

3. Generate database directory  
`$ mkdir mist/database`

4. Startup the microservice  
`$ docker-compose up --build`

Usage
-----
The microservice now runs on your localhost.
To use it there is a Jupyter notebook located in the `util` directory. Start up jupyter notebook server with  
`$ jupyter notebook`

Open the jupyter notebook and see local docs inside the notebook.

Access microservice database
----------------------------
Go to [mongo-express](http://localhost:8081) service on port 8081. You'll find your databases there, just click around.
